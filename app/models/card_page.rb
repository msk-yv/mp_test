class CardPage < ApplicationRecord
  attr_accessor :cards_list

  def cards_list
    cards.split('-').map(&:to_i)
  end

  def cards_list=(arr)
    send('cards=', arr.join('-'))
  end
end
